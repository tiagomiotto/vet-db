1-
CREATE VIEW dim_date AS
  (SELECT date_timestamp,
          year(date_timestamp),
          month(date_timestamp),
          day(date_timestamp)
   FROM consult);

2-
CREATE VIEW dim_animal AS
  (SELECT name AS animal_name,
          VAT AS animal_vat,
          species_name,
          age
   FROM animal);

3-
CREATE OR REPLACE VIEW facts_consult AS
  (SELECT animal_name AS name,
          animal_vat AS vat,
          dd.date_timestamp AS timestamp,

     (SELECT count(*)
      FROM procedures p
      WHERE p.name=animal_name
        AND p.date_timestamp = dd.date_timestamp) AS num_procedures,

     (SELECT count(*)
      FROM prescription pr
      WHERE pr.name=animal_name
        AND pr.date_timestamp = dd.date_timestamp) AS num_medications
   FROM dim_date dd,
        dim_animal da,
        consult c
   WHERE c.date_timestamp = dd.date_timestamp
     AND c.name = animal_name );




