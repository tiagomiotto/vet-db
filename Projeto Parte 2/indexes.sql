1 -
CREATE INDEX animal_idx_vat_name ON animal (VAT,name);
CREATE INDEX person_idx_name_vat ON person (name(50),VAT);
CREATE INDEX consult_idx_name_vet on consult (name, VAT_vet);


2-
CREATE INDEX indicator_idx_units_value ON indicator (units(50),reference_value);