1 - 
SELECT
  DISTINCT a.name AS "Animal Name",
  p.name AS "Owner name",
  a.species_name AS "Species",
  a.age AS "Age"
From
  animal a,
  person p,
  consult c
WHERE
  p.VAT = a.VAT
  AND a.name = c.name
  AND c.VAT_vet IN (
    SELECT
      VAT
    FROM
      person
    WHERE
      name = 'John Smith'
  );



2 - 
SELECT
  name,
  reference_value
FROM
  indicator
WHERE
  reference_value > 100
  and units = "miligrams"
ORDER BY
  reference_value DESC;




3 -
SELECT DISTINCT a.name AS "Animal name",
                c.name AS "Owner name",
                a.species_name AS "Species",
                a.age
                #c2.weight,
                #c2.date_timestamp
FROM animal a,
     person c,
     consult c2
WHERE a.name = c2.name
  AND a.VAT = c.VAT
  AND c2.weight > 30
  AND c2.date_timestamp >= ALL
    ( SELECT date_timestamp
     FROM consult c1
     WHERE c2.name = c1.name )
  AND ( c2.o LIKE "%obese%"
       OR c2.o LIKE "%obesity%" );


4 - 
SELECT
  name,
  VAT,
  address_street
FROM
  person p
WHERE
  not exists (
    SELECT
      VAT
    FROM
      animal a
    WHERE
      a.vat = p.VAT
  );



5 - 
SELECT
  d.code AS "Diagnosis Code",
  count(DISTINCT name_med) AS "Number different Medications"
FROM
  diagnosis_code d
  LEFT OUTER JOIN prescription p ON p.code = d.code
GROUP BY
  d.code;


6- 
SELECT avg_assists,
       avg_diagcode,
       avg_proce,
       avg_presc
FROM   (SELECT Count(p.vat_assistant) / Count(*) AS avg_assists
        FROM   consult c
               LEFT JOIN participation p
                      ON c.date_timestamp = p.date_timestamp
        WHERE  Year(c.date_timestamp) = 2017) AS avg_ass,
       (SELECT Count(d.code) / Count(DISTINCT c.date_timestamp) AS avg_diagcode
        FROM   consult c
               LEFT JOIN consult_diagnosis d
                      ON c.date_timestamp = d.date_timestamp
        WHERE  Year(c.date_timestamp) = 2017) AS avg_diagcod,
       (SELECT Count(p1.num) / Count(*) AS avg_proce
        FROM   consult c
               LEFT JOIN procedures p1
                      ON c.date_timestamp = p1.date_timestamp
        WHERE  Year(c.date_timestamp) = 2017) AS avg_proc,
       (SELECT Count(p1.regime) / Count(DISTINCT c.date_timestamp) AS avg_presc
        FROM   consult c
               LEFT JOIN prescription p1
                      ON ( c.date_timestamp = p1.date_timestamp
                           AND c.NAME = p1.NAME )
        WHERE  Year(c.date_timestamp) = 2017) AS avg_pres;  


7 - 
select
  species_name as Specie,
  name as Disease
from
  (
    select
      a.species_name,
      cd.code,
      dc.name
    from
      animal a,
      consult_diagnosis cd,
      generalization_species gs,
      diagnosis_code dc
    where
      gs.name1 = 'Dog'
      and gs.name2 = a.species_name
      and cd.name = a.name
      and dc.code = cd.code
    group by
      cd.code,
      a.species_name
    order by
      count(cd.code) Desc
  ) as X
group by
  species_name;



8 - 
SELECT
  p.name
FROM
  person p
WHERE
  (
    EXISTS (
      SELECT
        1
      FROM
        consult
      WHERE
        p.VAT = consult.VAT_owner
    )
    OR EXISTS (
      SELECT
        1
      FROM
        consult
      WHERE
        p.VAT = consult.VAT_client
    )
  )
  AND (
    EXISTS (
      SELECT
        1
      FROM
        consult
      WHERE
        p.VAT = consult.VAT_vet
    )
    OR EXISTS (
      SELECT
        1
      FROM
        assistant
      WHERE
        p.VAT = assistant.VAT
    )
  );





9 - 
SELECT
  c.name,
  c.address_street
FROM
  person c
WHERE
  c.vat IN (
    SELECT
      a.VAT
    FROM
      animal a
    WHERE
      a.VAT = c.VAT
      AND a.species_name LIKE "%Bird%"
      AND NOT EXISTS (
        SELECT
          a2.species_name
        FROM
          animal a2
        WHERE
          c.VAT = a2.VAT
          AND a2.species_name <> a.species_name
      )
  );