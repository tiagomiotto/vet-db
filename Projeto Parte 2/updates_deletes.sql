UPDATES:
1 - 
UPDATE
  person
SET
  address_street = "Av. Prof. Dr. Cavaco Silva",
  address_city = "Oerias"
WHERE
  name = "John Smith"
  AND person.VAT IN (
    SELECT
      VAT
    FROM
      client
  );


2 - 
UPDATE
  indicator
SET
  indicator.reference_value = indicator.reference_value * 1.1
WHERE
  name IN (
    SELECT
      p.indicator_name
    FROM
      test_procedure t,
      produced_indicator p
    WHERE
      p.date_timestamp = t.date_timestamp
      AND t.type = "blood"
  )
  AND units = "miligrams";


3 - 
DELETE FROM
  client
WHERE
  client.VAT IN (
    SELECT
      VAT
    FROM
      person
    WHERE
      name = "John Smith"
  );





4 - 
UPDATE
  consult_diagnosis, produced_indicator
SET
  consult_diagnosis.code = (
    SELECT
      code
    FROM
      diagnosis_code
    WHERE
      name = "end-stage renal disease"
  )
WHERE
  produced_indicator.indicator_name = "creatine"
  AND produced_indicator.value > 1
  AND consult_diagnosis.code = (
    SELECT
      code
    FROM
      diagnosis_code
    WHERE
      name = "kidney failure"
  );



