1.
SELECT COUNT(DISTINCT b.customer_name) FROM borrower b NATURAL JOIN customer c NATURAL JOIN loan l NATURAL JOIN branch br WHERE br.branch_city = c.customer_city;
+---------------------------------+
| count(distinct b.customer_name) |
+---------------------------------+
|                               1 |
+---------------------------------+

2.
SELECT d.customer_name, avg(a.balance) AS 'avg(balance)' FROM account a natural join depositor d group by d.customer_name;
+---------------+--------------+
| customer_name | avg(balance) |
+---------------+--------------+
| Hayes         |   400.000000 |
| Johnson       |   700.000000 |
| Jones         |   750.000000 |
| Lindsay       |   700.000000 |
| Smith         |   700.000000 |
| Turner        |   350.000000 |
+---------------+--------------+

3.
SELECT d.customer_name, avg(a.balance) AS 'avg(balance)' FROM account a natural join depositor d NATURAL JOIN branch b WHERE b.branch_city = 'Horseneck' GROUP BY d.customer_name;
+---------------+--------------+
| customer_name | avg(balance) |
+---------------+--------------+
| Hayes         |   400.000000 |
| Smith         |   700.000000 |
| Turner        |   350.000000 |
+---------------+--------------+

4.
SELECT sum(a.balance) AS 'sum(balance)' FROM account a NATURAL JOIN branch b WHERE b.branch_city = 'Horseneck';
+--------------+
| sum(balance) |
+--------------+
|      1450.00 |
+--------------+

5.
SELECT b.branch_city AS 'branch_city', sum(a.balance) AS 'sum(balance)' FROM account a NATURAL JOIN branch b GROUP BY b.branch_city;
+-------------+--------------+
| branch_city | sum(balance) |
+-------------+--------------+
| Brooklyn    |      2150.00 |
| Horseneck   |      1450.00 |
| Palo Alto   |       700.00 |
+-------------+--------------+

6.
SELECT branch_name FROM loan GROUP BY branch_name HAVING COUNT(*)>1 ORDER BY branch_name ASC;
+-------------+
| branch_name |
+-------------+
| Downtown    |
| Perryridge  |
+-------------+

7.
SELECT branch_name, sum(amount) FROM loan GROUP BY branch_name HAVING COUNT(*)>1 ORDER BY branch_name ASC;
+-------------+-------------+
| branch_name | sum(amount) |
+-------------+-------------+
| Downtown    |     2500.00 |
| Perryridge  |     2800.00 |
+-------------+-------------+

8.
SELECT b.branch_name AS 'branch_name', b.branch_city AS 'branch_city' FROM account a RIGHT OUTER JOIN branch b ON b.branch_name = a.branch_name WHERE a.account_number is NULL;
OU
SELECT b.branch_name AS 'branch_name', b.branch_city AS 'branch_city' FROM branch b LEFT OUTER JOIN account a ON b.branch_name = a.branch_name WHERE a.account_number is NULL;
+-------------+-------------+
| branch_name | branch_city |
+-------------+-------------+
| North Town  | Rye         |
| Pownal      | Bennington  |
+-------------+-------------+
 
9.
SELECT customer_name FROM customer WHERE customer_name NOT IN (SELECT customer_name FROM depositor);
+---------------+
| customer_name |
+---------------+
| Adams         |
| Brooks        |
| Curry         |
| Glenn         |
| Green         |
| Jackson       |
| Williams      |
+---------------+

10.
SELECT
  branch_name
FROM
  branch
WHERE
  branch_name NOT IN (
    SELECT
      branch_name
    FROM
      loan
    UNION
    SELECT
      branch_name
    FROM
      account
  );

+-------------+
| branch_name |
+-------------+
| North Town  |
| Pownal      |
+-------------+

11.
SELECT
  branch_name
FROM
  branch
WHERE
  branch_name NOT IN (
    SELECT
      branch_name
    FROM
      loan
    WHERE
      branch_name IN (
        SELECT
          branch_name
        FROM
          account
      )
  );
+-------------+
| branch_name |
+-------------+
| Brighton    |
| North Town  |
| Pownal      |
+-------------+


12. 
SELECT customer_name FROM customer WHERE customer_city IN (SELECT branch_city FROM branch);
+---------------+
| customer_name |
+---------------+
| Brooks        |
| Curry         |
| Jackson       |
| Johnson       |
| Smith         |
+---------------+

13. 
SELECT loan_number, amount FROM loan WHERE amount >= ALL (SELECT amount FROM loan);
+-------------+---------+
| loan_number | amount  |
+-------------+---------+
| L-23        | 2000.00 |
+-------------+---------+

14.
SELECT borrower.customer_name AS 'customer_name', sum(amount) FROM borrower NATURAL JOIN loan GROUP BY borrower.customer_name;
+---------------+-------------+
| customer_name | sum(amount) |
+---------------+-------------+
| Adams         |     1300.00 |
| Curry         |      500.00 |
| Hayes         |     1500.00 |
| Jackson       |     1500.00 |
| Jones         |     1000.00 |
| Smith         |     2900.00 |
| Williams      |     1000.00 |
+---------------+-------------+

15.     
SELECT customer_name FROM borrower NATURAL JOIN loan HAVING sum(amount) >= ALL (SELECT sum(amount) FROM borrower NATURAL JOIN loan GROUP BY customer_name);
SELECT customer_name FROM borrower NATURAL JOIN loan GROUP BY customer_name HAVING sum(amount) >= ALL (SELECT sum(amount) FROM borrower NATURAL JOIN loan GROUP BY customer_name);
SELECT customer_name FROM borrower NATURAL JOIN loan GROUP BY borrower.customer_name HAVING sum(loan.amount) >= ALL (SELECT sum(loan.amount) FROM borrower NATURAL JOIN loan GROUP BY borrower.customer_name);
                  
+---------------+
| customer_name |
+---------------+
| Smith         |
+---------------+
