1.3-
delimiter $$
CREATE TRIGGER procedures_before_insert BEFORE INSERT ON procedures 
FOR EACH ROW 
BEGIN
DECLARE new_num INT;
DECLARE aux_num INT;
select max(num) into aux_num from procedures;
set new_num = aux_num+1;
set new.num = new_num;
END;$$
delimiter ;

2.1 - Trigger idade

delimiter $$
create trigger compute_age BEFORE INSERT on animal
FOR EACH ROW
begin
DECLARE abirth INT;
DECLARE aage INT;
SELECT new.birth_year  INTO abirth;
SET aage = YEAR(CURDATE()) - abirth;
SET new.age = aage;
end;$$
delimiter ;

2.1.b 
delimiter $$
CREATE TRIGGER consult_age_update AFTER INSERT ON consult FOR EACH ROW BEGIN

DECLARE abirth INT;
DECLARE aage INT;
SELECT a.birth_year  INTO abirth from animal a where
a.VAT = new.VAT_owner and a.name = new.name;

SET aage = YEAR(CURDATE()) - abirth;

update animal a
set a.age = aage 
where a.VAT = new.VAT_owner and 
a.name = new.name;

END; $$
delimiter ;

2.2 

DELIMITER $$
CREATE TRIGGER `assist_not_vet` 
BEFORE INSERT ON `assistant` 
FOR EACH ROW 
BEGIN
    DECLARE num_vats int;
    SELECT count(*) into num_vats 
    from veterinary v
    where v.VAT = new.VAT;
    if (num_vats > 0) then
        signal sqlstate '45000' set message_text= 'The person already exists as a Veterinary';
    end if;
end;$$
DELIMITER ;


2.2.2
DELIMITER $$
CREATE TRIGGER `vet_not_assist` 
BEFORE INSERT ON `veterinary` 
FOR EACH ROW 
BEGIN
    declare num_vats int;
    SELECT count(*) into num_vats FROM assistant a
    WHERE a.VAT = new.VAT;
    IF (num_vats > 0) THEN
        signal sqlstate '45000' set message_text = 'The person already exists as an assistant';
    END if;
END;$$
DELIMITER ;

2.3
DELIMITER $$

CREATE TRIGGER `diff_phones` 
BEFORE INSERT ON `phone_number` 
FOR EACH ROW 
BEGIN
    declare num_phones int;
    SELECT count(*) into num_phones FROM phone_number pn
    WHERE pn.phone = new.phone;
    IF (num_phones > 0) THEN
        signal sqlstate '45000' set message_text = 'The phone number already belongs to another person';
    END if;
END;$$

DELIMITER ;



2.4
DELIMITER $$
CREATE FUNCTION num_consults (a_name VARCHAR(50), c_year INT, vat INT)
	returns INT
BEGIN
	DECLARE total INT;
	SELECT count(*) into total FROM consult WHERE name=a_name and VAT_owner = VAT and EXTRACT(YEAR FROM date_timestamp) = c_year;
	return total;
end;$$
DELIMITER ;


2.5
delimiter $$ 

create procedure change_units() begin
UPDATE
	produced_indicator
SET 
	produced_indicator.value = produced_indicator.value/10
WHERE
	produced_indicator.indicator_name IN (SELECT name FROM indicator WHERE indicator.units = "miligrams");

UPDATE
  indicator
SET
  indicator.reference_value = indicator.reference_value / 10,
  indicator.units = 'centigrams'
WHERE
  indicator.units = 'miligrams';
  end;$$ 
  delimiter ;








