SET FOREIGN_KEY_CHECKS = 0;
DROP TABLE IF EXISTS animal;
DROP TABLE IF EXISTS assistant;
DROP TABLE IF EXISTS client;
DROP TABLE IF EXISTS consult;
DROP TABLE IF EXISTS consult_diagnosis;
DROP TABLE IF EXISTS diagnosis_code;
DROP TABLE IF EXISTS dim_animal;
DROP TABLE IF EXISTS dim_date;
DROP TABLE IF EXISTS facts_consult;
DROP TABLE IF EXISTS generalization_species;
DROP TABLE IF EXISTS indicator;
DROP TABLE IF EXISTS medication;
DROP TABLE IF EXISTS participation;
DROP TABLE IF EXISTS performed;
DROP TABLE IF EXISTS person;
DROP TABLE IF EXISTS phone_number;
DROP TABLE IF EXISTS prescription;
DROP TABLE IF EXISTS produced_indicator;
DROP TABLE IF EXISTS radiography;
DROP TABLE IF EXISTS procedures;
DROP TABLE IF EXISTS species;
DROP TABLE IF EXISTS test_procedure;
DROP TABLE IF EXISTS veterinary;
SET FOREIGN_KEY_CHECKS = 1;


CREATE TABLE person (
  VAT int(11) unsigned NOT NULL,
  name text NOT NULL,
  address_street text NOT NULL,
  address_city text NOT NULL,
  address_zip int(11) NOT NULL,
  PRIMARY KEY (VAT)
);

CREATE TABLE diagnosis_code (
  code int(11) unsigned NOT NULL,
  name text NOT NULL,
  PRIMARY KEY (code)
);

CREATE TABLE indicator (
  name varchar(50) NOT NULL DEFAULT '',
  reference_value float NOT NULL,
  units text NOT NULL,
  description text NOT NULL,
  PRIMARY KEY (name)
);

CREATE TABLE medication (
  name varchar(50) NOT NULL DEFAULT '',
  lab varchar(50) NOT NULL DEFAULT '',
  dosage varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (name,lab,dosage)
);

CREATE TABLE species (
  name varchar(50) NOT NULL DEFAULT '',
  description text NOT NULL,
  PRIMARY KEY (name)
);

CREATE TABLE phone_number (
  phone int(11) unsigned NOT NULL,
  VAT int(11) unsigned NOT NULL,
  PRIMARY KEY (phone),
  FOREIGN KEY (VAT) REFERENCES person (VAT)
);

CREATE TABLE client (
  VAT int(11) unsigned NOT NULL,
  FOREIGN KEY (VAT) REFERENCES person (VAT) ON DELETE CASCADE
);

CREATE TABLE animal (
  name varchar(50) NOT NULL DEFAULT '',
  VAT int(11) unsigned NOT NULL,
  species_name varchar(50) NOT NULL DEFAULT '',
  colour text NOT NULL,
  gender char(1) NOT NULL DEFAULT '',
  birth_year year(4) NOT NULL,
  age int(11) NOT NULL,
  PRIMARY KEY (name,VAT),
  FOREIGN KEY (VAT) REFERENCES client (VAT) ON DELETE CASCADE,
  FOREIGN KEY (species_name) REFERENCES species (name)
);

CREATE TABLE assistant (
  VAT int(11) unsigned NOT NULL,
  FOREIGN KEY (VAT) REFERENCES person (VAT)
);

CREATE TABLE veterinary (
  VAT int(11) unsigned NOT NULL,
  specialization text NOT NULL,
  bio text NOT NULL,
  FOREIGN KEY (VAT) REFERENCES person (VAT)
);

CREATE TABLE consult (
  name varchar(50) NOT NULL DEFAULT '',
  VAT_owner int(11) unsigned NOT NULL,
  VAT_vet int(11) unsigned NOT NULL,
  VAT_client int(11) unsigned NOT NULL,
  date_timestamp timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  weight int(11) unsigned NOT NULL,
  s text NOT NULL,
  o text NOT NULL,
  a text NOT NULL,
  p text NOT NULL,
  PRIMARY KEY (date_timestamp),
  FOREIGN KEY (name) REFERENCES animal (name) ON DELETE CASCADE,
  FOREIGN KEY (VAT_client) REFERENCES client (VAT),
  FOREIGN KEY (VAT_owner) REFERENCES animal (VAT) ON DELETE CASCADE,
  FOREIGN KEY (VAT_vet) REFERENCES veterinary (VAT)
);

CREATE TABLE consult_diagnosis (
  code int(11) unsigned NOT NULL,
  name varchar(50) NOT NULL DEFAULT '',
  VAT_owner int(11) unsigned NOT NULL,
  date_timestamp timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  FOREIGN KEY (code) REFERENCES diagnosis_code (code),
  FOREIGN KEY (name) REFERENCES consult (name) ON DELETE CASCADE,
  FOREIGN KEY (date_timestamp) REFERENCES consult (date_timestamp),
  FOREIGN KEY (VAT_owner) REFERENCES consult (VAT_owner) ON DELETE CASCADE
);

CREATE TABLE generalization_species (
  name1 varchar(50) NOT NULL DEFAULT '',
  name2 varchar(50) NOT NULL DEFAULT '',
  FOREIGN KEY (name1) REFERENCES species (name) ON UPDATE CASCADE,
  FOREIGN KEY (name2) REFERENCES species (name) ON UPDATE CASCADE
);

CREATE TABLE participation (
  name varchar(50) NOT NULL DEFAULT '',
  VAT_owner int(11) unsigned NOT NULL,
  date_timestamp timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  VAT_assistant int(11) unsigned NOT NULL,
  FOREIGN KEY (date_timestamp) REFERENCES consult (date_timestamp),
  FOREIGN KEY (name) REFERENCES consult (name) ON UPDATE CASCADE,
  FOREIGN KEY (VAT_assistant) REFERENCES assistant (VAT),
  FOREIGN KEY (VAT_owner) REFERENCES consult (VAT_owner) ON DELETE CASCADE
);

CREATE TABLE procedures (
  name varchar(50) NOT NULL DEFAULT '',
  VAT_owner int(11) unsigned NOT NULL,
  date_timestamp timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  num int(11) unsigned NOT NULL,
  description text NOT NULL,
  PRIMARY KEY (num),
  FOREIGN KEY (date_timestamp) REFERENCES consult (date_timestamp),
  FOREIGN KEY (name) REFERENCES consult (name) ON DELETE CASCADE,
  FOREIGN KEY (VAT_owner) REFERENCES consult (VAT_owner) ON DELETE CASCADE
);

CREATE TABLE performed (
  name varchar(50) NOT NULL DEFAULT '',
  VAT_owner int(11) unsigned NOT NULL,
  date_timestamp timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  num int(11) unsigned NOT NULL,
  VAT_assistant int(11) unsigned NOT NULL,
  PRIMARY KEY (num),
  FOREIGN KEY (date_timestamp) REFERENCES procedures (date_timestamp),
  FOREIGN KEY (name) REFERENCES procedures (name),
  FOREIGN KEY (num) REFERENCES procedures (num),
  FOREIGN KEY (VAT_assistant) REFERENCES assistant (VAT),
  FOREIGN KEY (VAT_owner) REFERENCES procedures (VAT_owner) ON DELETE CASCADE
);

CREATE TABLE prescription (
  code int(11) unsigned NOT NULL,
  name varchar(50) NOT NULL DEFAULT '',
  VAT_owner int(11) unsigned NOT NULL,
  date_timestamp timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  name_med varchar(50) NOT NULL DEFAULT '',
  lab varchar(50) NOT NULL DEFAULT '',
  dosage varchar(50) NOT NULL DEFAULT '',
  regime text NOT NULL,
  FOREIGN KEY (code) REFERENCES consult_diagnosis (code) ON DELETE CASCADE ON UPDATE CASCADE,
  FOREIGN KEY (date_timestamp) REFERENCES consult_diagnosis (date_timestamp),
  FOREIGN KEY (name_med, lab, dosage) REFERENCES medication (name, lab, dosage),
  FOREIGN KEY (name) REFERENCES consult_diagnosis (name) ON DELETE CASCADE,
  FOREIGN KEY (VAT_owner) REFERENCES consult_diagnosis (VAT_owner) ON DELETE CASCADE
);

CREATE TABLE test_procedure (
  name varchar(50) NOT NULL DEFAULT '',
  VAT_owner int(11) unsigned NOT NULL,
  date_timestamp timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  num int(11) unsigned NOT NULL,
  type text,
  PRIMARY KEY (num),
  FOREIGN KEY (date_timestamp) REFERENCES procedures (date_timestamp),
  FOREIGN KEY (name) REFERENCES procedures (name),
  FOREIGN KEY (num) REFERENCES procedures (num),
  FOREIGN KEY (VAT_owner) REFERENCES procedures (VAT_owner) ON DELETE CASCADE
);

CREATE TABLE produced_indicator (
  name varchar(50) NOT NULL DEFAULT '',
  VAT_owner int(11) unsigned NOT NULL,
  date_timestamp timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  num int(11) unsigned NOT NULL,
  indicator_name varchar(50) NOT NULL DEFAULT '',
  value float NOT NULL,
  PRIMARY KEY (indicator_name, num, date_timestamp, VAT_owner, name),
  FOREIGN KEY (date_timestamp) REFERENCES test_procedure (date_timestamp),
  FOREIGN KEY (indicator_name) REFERENCES indicator (name),
  FOREIGN KEY (name) REFERENCES test_procedure (name),
  FOREIGN KEY (num) REFERENCES test_procedure (num),
  FOREIGN KEY (VAT_owner) REFERENCES test_procedure (VAT_owner) ON DELETE CASCADE
);

CREATE TABLE radiography (
  name varchar(50) NOT NULL DEFAULT '',
  VAT_owner int(11) unsigned NOT NULL,
  date_timestamp timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  num int(11) unsigned NOT NULL,
  file text,
  PRIMARY KEY (num),
  FOREIGN KEY (date_timestamp) REFERENCES procedures (date_timestamp),
  FOREIGN KEY (name) REFERENCES procedures (name),
  FOREIGN KEY (num) REFERENCES procedures (num),
  FOREIGN KEY (VAT_owner) REFERENCES procedures (VAT_owner) ON DELETE CASCADE
);


