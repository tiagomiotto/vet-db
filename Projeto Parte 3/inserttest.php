<html>
	<head>
		<title>Veterinario Central</title>
	    <meta charset="utf-8" />
	</head>

	<body>
			<?php
				$host = "db.tecnico.ulisboa.pt";
				$user = "g03159_SIBD";
				$pass = "87654321";
				$dsn = "mysql:host=$host;dbname=$user";
				try {
				    $connection = new PDO($dsn, $user, $pass, array(PDO::ATTR_ERRMODE => PDO::ERRMODE_WARNING));
				}
				catch (PDOException $exception) {
				    echo("<p>Error: ");
				    echo($exception->getMessage());
				    echo("</p>");
				    exit();
				}
						
				$animal_name = $_REQUEST['a_name'];
				$date_time = $_REQUEST['date'];
                $vat_owner = $_REQUEST['o_vat'];
                $vat_assist = $_REQUEST['a_vat'];
                if ($vat_assist == "null"){
	                $vat_assist='';
                }
                $white = $_REQUEST['white'];
                $neutro = $_REQUEST['neutro'];
                $lymp = $_REQUEST['lymp'];
                $mono = $_REQUEST['mono'];

			if($erb >= 0.0 && $ere >= 0.0 && $creatine >= 0.0){
				#echo("$animal_name $date_time $vat_owner ");
				$connection->beginTransaction();

				$stmt = $connection->prepare("INSERT INTO procedures VALUES 
                (:aname, :ovat, :datet, 'NULL','Blood Test')") ;
                //echo('<p>' . $sql . '</p>');
                
                $stmt->debugDumpParams();
                echo ("<br />\n");
                echo ("<br />\n");
                
                $stmt -> bindParam(':aname', $animal_name);
                $stmt -> bindParam(':ovat', $vat_owner);
                $stmt -> bindParam(':datet', $date_time);
                $stmt->execute();
                //$nrows = $connection->exec($sql);
                $nrows = $stmt->rowCount();	

                #Get Procedure NUM

                $stmt = $connection->prepare("SELECT num FROM procedures p WHERE 
                p.name = :aname and p.VAT_owner = :ovat and
                p.date_timestamp = :datet ORDER BY num DESC LIMIT 1
                ") ;
                
                $stmt->debugDumpParams();
                echo ("<br />\n");
                echo ("<br />\n");
                
                $stmt -> bindParam(':aname', $animal_name);
                $stmt -> bindParam(':ovat', $vat_owner);
                $stmt -> bindParam(':datet', $date_time);
                $stmt->execute();
                //$nrows = $connection->exec($sql);
                $nrows = $stmt->rowCount();	
                $temp = $stmt->fetch();
                $num = $temp['num'];
                
                $stmt = $connection->prepare("INSERT INTO test_procedure VALUES 
                (:aname,:ovat,:datet,'$num','Blood Test')");
                
                $stmt->debugDumpParams();
                echo ("<br />\n");
                echo ("<br />\n");
                
                $stmt -> bindParam(':aname', $animal_name);
                $stmt -> bindParam(':ovat', $vat_owner);
                $stmt -> bindParam(':datet', $date_time);
                $stmt->execute();

                $stmt = $connection->prepare("INSERT INTO produced_indicator VALUES 
                
                (:aname,:ovat,:datet,'$num','White blood cell count','$white'),
                (:aname,:ovat,:datet,'$num','Neutrophils','$neutro'),
                (:aname,:ovat,:datet,'$num','Lymphocytes','$lymp'),
                (:aname,:ovat,:datet,'$num','Monocytes','$mono')");
                
                $stmt->debugDumpParams();
                echo ("<br />\n");
                echo ("<br />\n");
                
                $stmt -> bindParam(':aname', $animal_name);
                $stmt -> bindParam(':ovat', $vat_owner);
                $stmt -> bindParam(':datet', $date_time);
                $stmt->execute();

                if($vat_assist != 0){

                    $sql = "SELECT VAT from assistant a where a.VAT = '$vat_assist';";
                    echo('<p>' . $sql . '</p>');
                    $result = $connection->query($sql);
                    $nrows = $result->rowCount();
                    if($nrows == 0){
                        echo("<p>This assistant doesn't exist!</p>");
                        $connection->rollback();
                        
                    }
                    else{
                        
	                    $stmt = $connection->prepare( "INSERT INTO performed VALUES 
	                    (:aname,:ovat,:datet,'$num','$vat_assist')");
	                    
	                    $stmt->debugDumpParams();
		                echo ("<br />\n");
		                echo ("<br />\n");
		                
		                $stmt -> bindParam(':aname', $animal_name);
		                $stmt -> bindParam(':ovat', $vat_owner);
		                $stmt -> bindParam(':datet', $date_time);
		                $stmt->execute();
					}
                }

                $connection->commit();
                echo("<p>Test procedure added!</p>");
                echo("<button onclick=\"location.href='checkanimal.php'\">Back</button>");
                
                $connection = null;
            }
                
                
            
        
        else{
            echo("<p>Please specify positive test results!</p>");
        }
			
			?>

	</body>
	
	
</html>