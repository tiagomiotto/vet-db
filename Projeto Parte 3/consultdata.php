<html>
	<head>
		<title>Veterinario Central</title>
	    <meta charset="utf-8" />
<!-- 	    <link rel="stylesheet" type="text/css" href="main.css"> -->
	</head>

	<body>
		
		<form action="createconsultForm.php?a_name=<?php echo($_REQUEST['a_name']); ?>&o_vat=<?php echo($_REQUEST['o_VAT']); ?>" method="post">
			<h3>Create consult for animal <?php echo($_REQUEST['a_name']); ?> </h3>
			<p><input type="submit" value="Create"/></p>
		</form>

			<?php
				$host = "db.tecnico.ulisboa.pt";
				$user = "g03159_SIBD";
				$pass = "87654321";
				$dsn = "mysql:host=$host;dbname=$user";
				try {
				    $connection = new PDO($dsn, $user, $pass);
				}
				catch (PDOException $exception) {
				    echo("<p>Error: ");
				    echo($exception->getMessage());
				    echo("</p>");
				    exit();
				}
				
						
				$animal_name = $_REQUEST['a_name'];
				$date_time = $_REQUEST['date'];
				$vat_owner = $_REQUEST['o_VAT'];
				
				#echo("$animal_name $date_time $vat_owner ");
				
				$stmt = $connection->prepare('SELECT distinct c.name as a_name, a.species_name as s_name, a.colour as color, a.gender as gender, a.birth_year as birth, 
				a.age as age, c.VAT_owner as o_vat, c.VAT_vet as v_vat, c.VAT_client as c_vat, 
				c.date_timestamp, c.s, c.o, c.a, c.p 
				from consult c, animal a 
				where c.date_timestamp = :date1
				and c.VAT_owner = :vat
				and c.name = :aname 
				and c.VAT_owner = a.VAT 
				and c.name = a.name;' );
				
				$stmt->debugDumpParams();
		        echo ("<br />\n");
		        echo ("<br />\n");
				
				$stmt -> bindParam(':date1', $date_time);
				$stmt -> bindParam(':vat', $vat_owner);
				$stmt -> bindParam(':aname', $animal_name);
				$stmt->execute();
				//$nrows = $connection->exec($sql);
				$nrows = $stmt->rowCount();	
				
				
				
				echo("<table border=\"1\">");
				echo("<tr><td>Animal Name</td><td>Species Name</td><td>Color</td><td>Gender</td><td>Birth Year</td><td>Age</td>
				<td>VAT Owner</td><td>VAT VET</td><td>VAT Client</td></tr>");
				foreach($stmt as $row){
					echo("<tr><td>"); echo($row['a_name']);
					echo("</td><td>"); echo($row['s_name']);
					echo("</td><td>"); echo($row['color']);
					echo("</td><td>"); echo($row['gender']);
					echo("</td><td>"); echo($row['birth']);
					echo("</td><td>"); echo($row['age']);
					echo("</td><td>"); echo($row['o_vat']);
					echo("</td><td>"); echo($row['v_vat']);
					echo("</td><td>"); echo($row['c_vat']);
					echo("</td></tr>");
				}
				echo("</table>");
				echo ("<br />\n");
				
				
				$stmt = $connection->prepare("SELECT distinct code as 'diagnose_code' 
				from consult_diagnosis cd
				where cd.date_timestamp = :date1
				and cd.VAT_owner = :vat
				and cd.name = :aname ");
				
				$stmt->debugDumpParams();
		        echo ("<br />\n");
		        echo ("<br />\n");
				
				$stmt -> bindParam(':date1', $date_time);
				$stmt -> bindParam(':vat', $vat_owner);
				$stmt -> bindParam(':aname', $animal_name);
				$stmt->execute();
			//$nrows = $connection->exec($sql);
				$nrows = $stmt->rowCount();	
				
				
				echo("<table border=\"1\">");
				echo("<tr><td>Diagnose code</td></tr>");
				if ($nrows > 0){
					foreach($stmt as $row){
						echo("<tr><td>"); echo($row['diagnose_code']);
						echo("</td></tr>");
					}
					echo("</table>");
					echo ("<br />\n");
				}
				else{
					echo("There are no diagnosys codes for this consult");
					echo ("<br />\n");
				}
				
				
				$stmt = $connection->prepare( "SELECT distinct cd.code as code, p.name_med as med, p.lab as lab, p.dosage as dosage, p.regime as regime
				from prescription p, consult_diagnosis cd
				where p.date_timestamp = :date1
				and p.VAT_owner = :vat
				and p.name = :aname
				and p.code = cd.code;");
				
				$stmt->debugDumpParams();
		        echo ("<br />\n");
		        echo ("<br />\n");
				
				$stmt -> bindParam(':date1', $date_time);
				$stmt -> bindParam(':vat', $vat_owner);
				$stmt -> bindParam(':aname', $animal_name);
				$stmt->execute();
				
				
				echo("<table border=\"1\">");
				echo("<tr><td>Diagnose code</td><td>Medicine</td><td>Lab</td><td>Dosage</td><td>regime</td></tr>");
				if ($nrows > 0){
					foreach($stmt as $row){
						echo("<tr><td>"); echo($row['code']);
						echo("</td><td>"); echo($row['med']);
						echo("</td><td>"); echo($row['lab']);
						echo("</td><td>"); echo($row['dosage']);
						echo("</td><td>"); echo($row['regime']);
						echo("</td></tr>");
					}
					echo("</table>");
					echo ("<br />\n");
				}
				else{
					echo("There are no prescriptions for this consult");
				}

				
				$connection = null;
			
			?>

	</body>
	
	
</html>