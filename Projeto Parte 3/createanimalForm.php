<html>
	<head>
		<title>Veterinario Central</title>
	</head>
	<body>
		<h3>Create Animal <?=$_REQUEST['name']?></h3>
		<form action="createanimal.php" method="post">
			<p><input type="hidden" name="animal_name" value="<?=$_REQUEST['name']?>"/></p>
			<p><input type="hidden" name="VAT_client" value="<?=$_REQUEST['VAT_client']?>"/></p>
			<p>Species Name:
				<select name="species">
					<?php
						$host = "db.tecnico.ulisboa.pt";
						$user = "g03159_SIBD";
						$pass = "87654321";
						$dsn = "mysql:host=$host;dbname=$user";
						try {
						    $connection = new PDO($dsn, $user, $pass);
						}
						catch (PDOException $exception) {
						    echo("<p>Error: ");
						    echo($exception->getMessage());
						    echo("</p>");
						    exit();
						}
						$sql = "SELECT name FROM species;";
						$result = $connection->query($sql);
						foreach($result as $row){
							unset($name);
							$name = $row['name']; 
							echo '<option value="'.$name.'">'.$name.'</option>';
						}
					?>
				</select>
			</p>
			<p>Color: <input type="text" name="color" pattern="[A-Za-z]+" maxlength="50" required/></p>
			<p>Gender: 
				<select name="gender">
				  <option value="M">Male</option>
				  <option value="F">Female</option>
				</select>
			</p>
			<p>Birth Year: <input type="number" name="birth" max="<?php echo date("Y"); ?>" pattern="[0-9]+" maxlength="4" required/></p>
			<p><input type="submit" value="Create"/></p>	
		</form>	
	</body>
	
</html>
