INSERT INTO person VALUES (1111,'Joaquim','Rua das batatas','Cidade das couves',1515),
						  (1223,'Sebastiao','Rua das Flores','Lisboa',1122),
						  (1234,'John Smith','Av. Rovisco Pais','Liboa',1000),
						  (1526,'Al Gore','San Lorenzo','CA',3000),
						  (1567,'Basile','Rua das Aves','Porto',1153),
						  (1717,'Bolsonaro','Av. do Brasil','Lisboa',1144),
						  (3345,'Joao Tomas','Rua do Tomar','Lisboa',1122),
						  (4321,'Tiago','Av. Rovico Pais','Lisboa',1100),
						  (5555,'Kellen','Av. dos Gatos','Santarem',1333),
						  (6666,'Trump','Rua dos anjos','Ponta Delgada',4646);


INSERT INTO diagnosis_code VALUES (1,'Inflamacao'),
								  (33,'Plaquetas baixas'),
								  (345,'Osteoporose'),
								  (666,'Cancer'),
								  (849,'kidney failure'),
								  (999,'end-stage renal disease'),
								  (1234,'Osso quebrado');


INSERT INTO indicator VALUES ('White blood cell count',1000,'#','White blood cell count'),
							 ('Neutrophils',5000,'#','number of Neutrophils'),
							 ('Lymphocytes',4000,'#','number of Lymphocytes'),
							 ('Monocytes',4000,'#','number of Monocytes'),
							 ('creatine',0.3,'mg','Creatina'),
							 ('ERB',100,'miligrams','Exame ERB'),
							 ('ERE',150,'miligrams','Exame ERA'),
							 ('LGB',13.2,'miligrams','Exame LGB'),
							 ('PSA',0.4,'miligrams','Exame PSA'),
							 ('Ureia',0.3,'mg/ml','Exame Ureia');


INSERT INTO medication VALUES ('Alozapram','OPD','100mg'),
							  ('Brufen','OSF','100mg'),
							  ('Calcio','MMJ','200mg'),
							  ('Dimeticona','Ache','40mg'),
							  ('Morfina','OPP','100ml'),
							  ('Oxycotin','Esper','40mg'),
							  ('Vitamina C','Porto','200ml'),
							  ('Vonau','KKL','20mg');


INSERT INTO species VALUES ('Bird','Animais com asas'),
						   ('Dog','Cachorro'),
						   ('Felino','Gato'),
						   ('Mamifero','Mamals'),
						   ('Papagaio','ave chata'),
						   ('Pastor Alemao','Cachorro Grande'),
						   ('Pug','Cachorro gordo'),
						   ('Reptil','Cobra');


INSERT INTO phone_number VALUES (921548732,1526),
								(915833925,4321),
								(666666666,6666);


INSERT INTO client VALUES (1111),
						  (1223),
						  (1234),
						  (1567),
						  (1717),
						  (3345),
						  (4321),
						  (5555),
						  (6666);


INSERT INTO animal VALUES ('Bongo',1567,'Pastor Alemao','Branco','M',2007,11),
						  ('Carson',1223,'Felino','Preto','M',1999,19),
						  ('Cleison',5555,'Pastor Alemao','Preto','M',2015,3),
						  ('Dinis',1223,'Pug','amarelo','F',2016,2),
						  ('Geremias',1234,'Bird','Amarelo','M',2011,9),
						  ('Hillary',6666,'Reptil','rosa','F',2017,1),
						  ('Joao',1223,'Pug','amarelo','F',2016,2),
						  ('malandro',1111,'Bird','arco iris','F',2000,18),
						  ('Pugzila',5555,'Pug','preto','M',2014,4),
						  ('Roberto',4321,'Bird','Brown','M',2013,5),
						  ('Joao',6666,'Reptil','cinza','M',2015,3);


INSERT INTO assistant VALUES (1111),
							 (1567);


INSERT INTO veterinary VALUES (1234,'General','Formado na ULisboa'),
							  (4321,'Aves','Faculdade de Veterinaria'),
							  (1717,'General','President by day, Veterinary by night');


INSERT INTO consult VALUES ('Joao',4321,1234,4321,'2017-11-05 21:30:43',32,'Esta Bem','pulsacao normal','Consulta de rotina tudo OK ','Pesar, medir, exame urina'),
						   ('Hillary',6666,1234,1223,'2017-11-05 21:43:25',25,'A passar mal','subnutrida','falta de vitaminas A,B,C,D','analise de sangue'),
						   ('Carson',1223,4321,1223,'2017-11-05 21:55:19',29,'Esta ok','tem 3 metros','consulta de rotina tudo ok','pesar, medir temperatura, dar vacinas'),
						   ('Dinis',1223,1234,6666,'2017-11-05 22:24:43',15,'Esta abaixo do peso','Esta coxo','Possivel inflamacao devido a queda','pesar e fazer raio x'),
						   ('Roberto',4321,4321,3345,'2017-11-05 23:51:14',35,'A passar mal','pulsacao baixa e obesity','obesity','Fazer exame de sangue'),
						   ('Joao',1223,1234,4321,'2018-11-05 21:33:49',25,'parece mais ou menos','tem derrame no olho','esta bem mas precisa de limpeza no olho','precisa de limpeza ocular'),
						   ('Carson',1223,4321,1223,'2018-11-05 21:34:44',31,'Esta em condicoes','esta tudo bem','consulta de rotina tudo ok','pesar, medir temperatura, dar vacinas'),
						   ('Cleison',5555,1234,6666,'2018-11-05 22:16:09',54,'Esta abaixo do peso','Pelo esta bem grande','consulta de rotina ok','pesar e fazer raio x'),
						   ('Pugzila',5555,1234,1567,'2018-11-05 22:28:52',75,'esta gordo','tem 30 kilos a mais','parece comer demasiado','pesar'),
						   ('Bongo',1567,1234,6666,'2018-11-05 22:29:57',54,'Esta abaixo do peso','AParenta estar fraco','consulta de rotina ok','pesar e fazer exame de urina'),
						   ('Dinis',1223,1234,5555,'2018-11-05 23:48:24',32,'um pouco obese','tem mais 3 cm de pansa ou seja obese','deve comecar dieta','pesar, alimentar, massajar'),
						   ('Roberto',4321,4321,3345,'2018-11-07 16:10:47',35,'A passar mal','obesity','Deve tomar remedio para a pressao','Fazer exame de sangue'),
						   ('Geremias',1234,4321,1234,'2018-11-09 01:10:38',1,'OK','ok','ok','ok'),
						   ('Hillary',6666,1234,1111,'2017-11-29 17:08:58',20,'coconut','hurt','great','go have fun');


INSERT INTO consult_diagnosis VALUES (1,'Hillary',6666,'2017-11-05 21:43:25'),
									 (33,'Hillary',6666,'2017-11-05 21:43:25'),
									 (849,'Roberto',4321,'2018-11-07 16:10:47'),
									 (666,'Cleison',5555,'2018-11-05 22:16:09'),
									 (1,'Bongo',1223,'2018-11-05 22:29:57'),
									 (666,'Bongo',1223,'2018-11-05 22:29:57'),
									 (1234,'Pugzila',5555,'2018-11-05 22:28:52'),
									 (1,'Dinis',1223,'2017-11-05 22:24:43'),
									 (1,'Joao',1223,'2017-11-05 21:30:43'),
									 (345,'Cleison',5555,'2018-11-05 22:16:09'),
									 (1,'Geremias',1234,'2017-11-05 21:30:43'),
									 (1,'Roberto',4321,'2017-11-05 23:51:14');


INSERT INTO generalization_species VALUES ('Mamifero','Dog'),
										  ('Mamifero','Felino'),
										  ('Dog','Pug'),
										  ('Dog','Pastor Alemao'),
										  ('Bird','Papagaio');


INSERT INTO participation VALUES ('Roberto',4321,'2017-11-05 23:51:14',1567),
								 ('Dinis',1223,'2017-11-05 22:24:43',1111),
								 ('Pugzila',5555,'2018-11-05 22:28:52',1567);


INSERT INTO procedures VALUES ('Hillary',6666,'2017-11-05 21:43:25',1,'heart transplant'),
							  ('Roberto',4321,'2017-11-05 23:51:14',2,'blood test'),
							  ('Carson',1223,'2017-11-05 21:55:19',3,'Radiografia'),
							  ('Geremias',1234,'2017-11-05 21:30:43',4,'TESTE');


INSERT INTO performed VALUES ('Hillary',6666,'2017-11-05 21:43:25',1,1111),
							 ('Roberto',4321,'2017-11-05 23:51:14',2,1567);


INSERT INTO prescription VALUES (849,'Hillary',6666,'2017-11-05 21:43:25','Morfina','OPP','100ml','2x ao dia as refeicoes'),
								(33,'Hillary',6666,'2017-11-05 21:43:25','Alozapram','OPD','100mg','5x ao dia'),
								(849,'Roberto',4321,'2017-11-05 23:51:14','Vitamina C','Porto','200ml','1x dia'),
								(33,'Geremias',1234,'2017-11-05 21:30:43','Vitamina C','Porto','200ml','1x dia');


INSERT INTO test_procedure VALUES ('Roberto',4321,'2017-11-05 23:51:14',2,'blood');


INSERT INTO produced_indicator VALUES ('Roberto',4321,'2017-11-05 23:51:14',2,'creatine',1.2),
									  ('Roberto',4321,'2017-11-05 23:51:14',2,'ERB',100),
									  ('Roberto',4321,'2017-11-05 23:51:14',2,'ERE',150);


INSERT INTO radiography VALUES ('Carson',1223,'2017-11-05 21:55:19',1,'/Desktop/Radiografias/Carson');



