<!DOCTYPE html>
<html>
	<head>
		<title>Veterinario Central</title>
	</head>
	
	<body>
		<?php
		
			$host = "db.tecnico.ulisboa.pt";
			$user = "g03159_SIBD";
			$pass = "87654321";
			$dsn = "mysql:host=$host;dbname=$user";
			try {
				$connection = new PDO($dsn, $user, $pass, array(PDO::ATTR_ERRMODE => PDO::ERRMODE_WARNING));
			}
			catch (PDOException $exception) {
			    echo("<p>Error: ");
			    echo($exception->getMessage());
			    echo("</p>");
			    exit();
			}
			
			$VAT_client = $_POST['VAT_client'];
			$temp =  "1223";
			$animal_name = $_POST['animal_name'];
			$owner_name = $_POST['owner_name'];
			
			$stmt = $connection->prepare('SELECT DISTINCT cl.VAT, a.name as animal_name, p.name as owner_name 
			FROM consult c, person p , client cl, 
			animal a where a.name = :name1 and a.VAT = p.VAT and p.name 
			LIKE :name2 and ((cl.VAT in (select 
			c.VAT_Client from consult c where c.name = a.name 
			and c.VAT_owner = p.VAT) and cl.VAT = :name3 ))' );
			
			$stmt->debugDumpParams();
            echo ("<br />\n");
            echo ("<br />\n");
			
			$owner_name="%".$owner_name."%"; 
			
			$stmt -> bindParam(':name1', $animal_name);
			$stmt -> bindParam(':name2', $owner_name);
			$stmt -> bindParam(':name3', $VAT_client);
	
			$stmt->execute();
			$nrows = $stmt->rowCount();	

			if ($nrows == 0) {
				$stmt = $connection->prepare('SELECT DISTINCT a.name as animal_name FROM person p, animal a 
				where a.name = :name1 and a.VAT = p.VAT and p.name LIKE :name2');
				
				$stmt->debugDumpParams();
                echo ("<br />\n");
                echo ("<br />\n");
				
				#$owner_name="%".$owner_name."%"; 
			
				$stmt -> bindParam(':name1', $animal_name);
				$stmt -> bindParam(':name2', $owner_name);
				
		
				$stmt->execute();
				$nrows = $stmt->rowCount();	
				if($nrows == 0 ){
					echo("The Animal $animal_name does not exists, create?");
					echo("<button onclick=\"location.href='createanimalForm.php?name=$animal_name&VAT_client=$VAT_client'\">Create</button>");
					
					echo("<button onclick=\"location.href='checkanimal.php'\">Back</button>");
				}
				else{
					$stmt = $connection->prepare('SELECT VAT FROM client WHERE VAT = :name3');
					
					$stmt -> bindParam(':name3', $VAT_client);
			
					$stmt->execute();
					$nrows = $stmt->rowCount();	
					if($nrows == 0 ){
						echo("The Client $VAT_client does not exists, create?");
						echo("<button onclick=\"location.href='createclient.php?client=$VAT_client'\">Create</button>");
						echo("<button onclick=\"location.href='checkanimal.php'\">Back</button>");
					}
					else{
						$stmt = $connection->prepare('SELECT DISTINCT a.name as animal_name, p.name as owner_name FROM person p, consult a 
						where a.name = :name1 and a.VAT_owner = p.VAT and p.name LIKE :name2');
						
						$stmt->debugDumpParams();
						echo ("<br />\n");
						echo ("<br />\n");
						
						#$owner_name='%'.$owner_name.'%';   COPY PASTE
						
						$stmt -> bindParam(':name1', $animal_name);
						$stmt -> bindParam(':name2', $owner_name);
						#$stmt -> bindParam(':name3', $VAT_client);
				
						$stmt->execute();
						$nrows = $stmt->rowCount();	
						echo("The client $VAT_client has never taken $animal_name to any consult!");
						echo("<table border=\"1\">");
						echo("<tr><td>VAT Client</td><td>Animal Name</td><td>Owner Name</td></tr>");
						foreach($stmt as $row){			# AQUI ESTA RESULT EM VEZ DE STMT
							echo("<tr><td>"); echo(NULL); echo('</td>');
							echo("<td><a href=\"checkconsult.php?a_name=");
						    echo($row['animal_name']);
						    echo("&o_name=");
						    echo($row['owner_name']);
						    echo("&c_VAT=");
						    echo(NULL);
						    echo("\">");
						    echo($row['animal_name']);
						    echo("</a></td>\n");
							echo("</td><td>"); echo($row['owner_name']);
							echo("</td></tr>");
						}
						echo("</table>");;
					}
					
				}
			}
			else {
				echo("<table border=\"1\">");
				echo("<tr><td>VAT Client</td><td>Animal Name</td><td>Owner Name</td></tr>");
				foreach($stmt as $row){
					echo("<tr><td>"); echo($row['VAT']); echo('</td>');
					echo("<td><a href=\"checkconsult.php?a_name=");
				    echo($row['animal_name']);
				    echo("&o_name=");
				    echo($row['owner_name']);
				    echo("&c_VAT=");
				    echo($row['VAT']);
				    echo("\">");
				    echo($row['animal_name']);
				    echo("</a></td>\n");
					echo("</td><td>"); echo($row['owner_name']);
					echo("</td></tr>");
				}
				echo("</table>");
			}
			$connection = null;
		?>
	</body>
	
</html>
