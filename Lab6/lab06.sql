	
1.
SELECT b.branch_name FROM branch b WHERE b.branch_city = 'Brooklyn';
+-------------+
| branch_name |
+-------------+
| Brighton    |
| Downtown    |
+-------------+

2.
SELECT
  b.branch_name,
  a.account_number
FROM
  branch b
  LEFT OUTER JOIN account a ON a.branch_name = b.branch_name
WHERE
  b.branch_city = 'Brooklyn';


+-------------+----------------+
| branch_name | account_number |
+-------------+----------------+
| Brighton    | A-201          |
| Brighton    | A-217          |
| Downtown    | A-101          |
+-------------+----------------+

3.
SELECT
  b.branch_name,
  a.account_number,
  d.customer_name
FROM
  branch b
  LEFT OUTER JOIN account a ON a.branch_name = b.branch_name
  LEFT OUTER JOIN depositor d ON d.account_number = a.account_number
WHERE
  b.branch_city = 'Brooklyn';
+-------------+----------------+---------------+
| branch_name | account_number | customer_name |
+-------------+----------------+---------------+
| Brighton    | A-201          | Johnson       |
| Brighton    | A-217          | Jones         |
| Downtown    | A-101          | Johnson       |
+-------------+----------------+---------------+

5.
select
  distinct customer_name
from
  depositor as d
where
  not exists (
    select
      branch_name
    from
      branch as b
    where
      branch_city = 'Brooklyn'
      and branch_name not in (
        select
          branch_name
        from
          account as a,
          depositor as d2
        where
          a.account_number = d2.account_number
          and d2.customer_name = d.customer_name
      )
  );

+---------------+
| customer_name |
+---------------+
| Johnson       |
+---------------+

7.
select
  distinct customer_name
from
  depositor as d
where
  not exists (
    select
      branch_name
    from
      branch as b
    where
      branch_city = 'Brooklyn'
      and not exists (
        select
          *
        from
          account as a,
          depositor as d2
        where
          a.account_number = d2.account_number
          and d2.customer_name = d.customer_name
          and a.branch_name = b.branch_name
      )
  );

9.
select
  distinct customer_name
from
  depositor as d NATURAL JOIN customer as c
where
  not exists (
    select
      branch_name
    from
      branch as b
    where
      branch_city = customer_city
      and not exists (
        select
          *
        from
          account as a,
          depositor as d2
        where
          a.account_number = d2.account_number
          and d2.customer_name = d.customer_name
          and a.branch_name = b.branch_name
      )
  );


10.
select
  c.customer_name,
  c.customer_city,
  b.branch_city
from
  customer as c,
  depositor as d,
  account as a,
  branch as b
where
  c.customer_name = d.customer_name
  and d.account_number = a.account_number
  and a.branch_name = b.branch_name
  and c.customer_name in ('Hayes', 'Jones', 'Lindsay', 'Turner');


+---------------+---------------+-------------+
| customer_name | customer_city | branch_city |
+---------------+---------------+-------------+
| Hayes         | Harrison      | Horseneck   |
| Jones         | Harrison      | Brooklyn    |
| Lindsay       | Pittsfield    | Palo Alto   |
| Turner        | Stamford      | Horseneck   |
+---------------+---------------+-------------+



11.
select
  distinct customer_name
from
  depositor as d NATURAL JOIN customer as c
where
  not exists (
    select
      branch_name
    from
      branch as b
    where
      branch_city = customer_city
      and not exists (
        select
          *
        from
          account as a,
          depositor as d2
        where
          a.account_number = d2.account_number
          and d2.customer_name = d.customer_name
          and a.branch_name = b.branch_name
      )
  )
  and customer_city in (
  	select
		branch_city
  	from
		branch
  	where
		branch_city = customer_city

   );


