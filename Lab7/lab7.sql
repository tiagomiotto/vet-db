1 - 

drop function absolute_balance;
delimiter $$
create function absolute_balance(a_customer_name varchar(255))
returns decimal(20,2)
begin
declare a_balance decimal(20,2);
declare a_loan decimal(20,2);
declare total decimal(20,2);
select sum(balance) into a_balance from account natural join depositor d where d.customer_name = a_customer_name;
if a_balance is null then SET a_balance = 0; end if;
select sum(amount) into a_loan from borrower b natural join loan l where b.customer_name = a_customer_name;
if a_loan is null then SET a_loan = 0; end if;
set total = a_balance - a_loan;
return total;
end$$
delimiter ;

3 - 

SELECT customer_name, absolute_balance(customer_name) FROM customer where absolute_balance(customer_name) >= ALL (Select absolute_balance(customer_name) From customer);

4 - 

delimiter $$
drop procedure if exists list_cutomers
create procedure list_cutomers(in b_name varchar(255))
begin
select customer_name from depositor as d, account as a
where d.account_number = a.account_number and a.branch_name = b_name;
end$$
delimiter ;

call list_cutomers('Brighton');


6 -
delimiter $$
create trigger check_balance before update on loan 
for each row
begin
	if new.amount < 0 then
		insert into account values (new.loan_number,new.branch_name, (-1)*new.amount);
		insert into depositor (
			select customer_name, loan_number
			from borrower as b
			where b.loan_number = new.loan_number);
		set new.amount = 0; 
		end if;
end$$ 
delimiter ;
