Pergunta 1:
MySQL [ist182045]> SELECT DISTINCT depositor.customer_name FROM account, depositor WHERE account.account_number = depositor.account_number and account.balance > 500;
+---------------+
| customer_name |
+---------------+
| Johnson       |
| Smith         |
| Jones         |
| Lindsay       |
+---------------+
4 rows in set (0.00 sec)

Pergunta 2:

MySQL [ist182045]> SELECT DISTINCT c.customer_city FROM customer c, borrower b, loan l WHERE l.loan_number = b.loan_number and b.customer_name=c.customer_name and  l.amount between 1000 and 2000;
+---------------+
| customer_city |
+---------------+
| Brooklyn      |
| Harrison      |
| Pittsfield    |
| Princeton     |
| Rye           |
+---------------+
5 rows in set (0.01 sec)

Pergunta 3 - 
MySQL [ist182045]> SELECT a.balance * 0.99 FROM account a WHERE branch_name="Perryridge";
+------------------+
| a.balance * 0.99 |
+------------------+
|         396.0000 |
+------------------+
1 row in set (0.00 sec)

Pergunta 4-

MySQL [ist182045]> SELECT a.account_number ,a.balance FROM account a, depositor d, customer c, borrower b WHERE a.account_number = d.account_number and d.customer_name = c.customer_name and b.customer_name =c.customer_name and b.loan_number="L-15";
+----------------+---------+
| account_number | balance |
+----------------+---------+
| A-102          |  400.00 |
+----------------+---------+
1 row in set (0.00 sec)

Pergunta 5-

MySQL [ist182045]> SELECT DISTINCT c.customer_name FROM customer c, branch b WHERE c.customer_city=b.branch_city;
+---------------+
| customer_name |
+---------------+
| Brooks        |
| Curry         |
| Jackson       |
| Johnson       |
| Smith         |
+---------------+
5 rows in set (0.00 sec)

Pergunta 6-

MySQL [ist182045]> SELECT b.assets FROM depositor d, account a, branch b WHERE d.customer_name="Jones" AND d.account_number=a.account_number and a.branch_name=b.branch_name;
+------------+
| assets     |
+------------+
| 7100000.00 |
+------------+
1 row in set (0.00 sec)

Pergunta 7- 
MySQL [ist182045]> SELECT a.branch_name FROM account a, depositor d WHERE d.customer_name like 'J%S' AND d.account_number=a.account_number;
+-------------+
| branch_name |
+-------------+
| Brighton    |
+-------------+
1 row in set (0.00 sec)

Pergunta 8-

MySQL [ist182045]> SELECT c.customer_name, c.customer_street, l.loan_number, l.amount FROM customer c, borrower b, loan l WHERE customer_street like '____' AND c.customer_name=b.customer_name AND b.loan_number=l.loan_number;
+---------------+-----------------+-------------+---------+
| customer_name | customer_street | loan_number | amount  |
+---------------+-----------------+-------------+---------+
| Hayes         | Main            | L-15        | 1500.00 |
| Jones         | Main            | L-17        | 1000.00 |
+---------------+-----------------+-------------+---------+
2 rows in set (0.00 sec)

Pergunta 9-
MySQL [ist182045]> SELECT b.customer_name FROM borrower b, depositor d, account a, loan l WHERE b.customer_name = d.customer_name AND d.account_number = a.account_number AND b.loan_number = l.loan_number AND a.branch_name = l.branch_name;
+---------------+
| customer_name |
+---------------+
| Hayes         |
+---------------+
1 row in set (0.00 sec)


